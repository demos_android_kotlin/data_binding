package com.ejemplo.data.binding

class Usuario {

    var nombre: String? = ""
    var apellido: String? = ""
    var telefono: String? = ""

    //constructor()

    constructor(nombre: String?, apellido: String?, telefono: String?) {
        this@Usuario.nombre = nombre
        this@Usuario.apellido = apellido
        this@Usuario.telefono = telefono
    }

    constructor(nombre: String?, apellido: String?) {
        this@Usuario.nombre = nombre
        this@Usuario.apellido = apellido
    }
}



