package com.ejemplo.data.binding

import android.databinding.DataBindingUtil
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import com.ejemplo.data.binding.databinding.ActivityMainBinding

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        init()
    }

    private fun init() {
        var usuario = Usuario("Jorge Luis", "Villavicencio Correa", "945019089")

        var binding: ActivityMainBinding? = DataBindingUtil.setContentView(this@MainActivity, R.layout.activity_main)

        binding?.setUsuario(usuario)
    }
}
